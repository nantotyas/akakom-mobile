package id.adc.akakommobile.fragment;

import id.adc.akakommobile.R;
import id.adc.akakommobile.lib.TextViewEx;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MIFragment extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_mi, container, false);
		
		TextViewEx samMI = (TextViewEx) view.findViewById(R.id.textMI);
		samMI.setText("Jurusan/program studi Manajemen Informatika dengan jenjang D3 diharapkan dapat memberikan kontribusi untuk mewujudkan kebutuhan tenaga-tenaga siap pakai pada semua bidang TI. Dalam mewujudkan harapan tersebut maka jurusan/program studi Manajemen Informatika membuat kurikulum yang berbasis kompetensi. Sesuai dengan Keputusan Menteri Pendidikan Nasional RI No. 232 Tahun 2000 tentang Pedoman Penyusunan Kurikulum Pendidikan Tinggi dan Penilaian Hasil Belajar Mahasiswa, jurusan/program studi Manajemen Informatika jenjang D3 (MI-D3), pada tahun akademik 2004/2005 mengikuti pola yang dianjurkan oleh  SK Mendiknas tersebut. Setelah melalui proses yang cukup panjang, kurikulum berbasis kompetensi program studi MI diberlakukan bagi semua mahasiswa MI yang belum menyelesaikan studinya hingga tahun akademik 2003/2004. "
				+ "Selain hasil kurikulum yang dituangkan dalam bentuk distribusi semesteran, pengembangan isi dan materi silabi juga diberlakukan.", true);
		
		return view;
	}

}
