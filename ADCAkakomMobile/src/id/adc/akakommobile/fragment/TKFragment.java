package id.adc.akakommobile.fragment;

import id.adc.akakommobile.R;
import id.adc.akakommobile.lib.TextViewEx;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TKFragment extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_tk, container, false);
		
		TextViewEx samTk = (TextViewEx) view.findViewById(R.id.textTK);
		samTk.setText("Mahasiswa program ini disarankan menyelesaikan beban akademik 110 sks dan latihan kerja dalam rentang 6 semester. Frame akademik dalam program ini sarat kegiatan praktek dengan peranti hi-tech measurement dan device programming, sehingga mahasiswa dituntut tekun dan berlatih mengembangkan logika. Alur matakuliah dan praktik yang ada mengindikasikan kepada konsentrasiInstrumentasi, Future Control, dan Jaringan Computer. Pada akhir masa studi, mahasiswa dilatih menyelesaikan laporan tugas akhir, yang menunjukkan penguasaan penyelesaian pekerjaan tertentu dalam lingkup yang disebut di muka. "
				+ "Mahasiswa yang mampu dapat melanjutkan studi pada jenjang selanjutnya (D4/S1), bidang yang sesuai umumnya adalah dengan lingkup keteknikan dan systems.", true);
		
		return view;
	}

}
