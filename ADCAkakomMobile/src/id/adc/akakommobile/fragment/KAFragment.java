package id.adc.akakommobile.fragment;

import id.adc.akakommobile.R;
import id.adc.akakommobile.lib.TextViewEx;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class KAFragment extends Fragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_ka, container, false);
		
		TextViewEx samKa = (TextViewEx) view.findViewById(R.id.textKA);
		samKa.setText("Program studi Komputerisasi Akuntansi di STMIK AKAKOM didirikan pada tahun 1999. Pada saat berdiri, program studi Komputerisasi Akuntansi memperoleh status Terdaftar berdasarkan SK Dirjen Dikti Nomor 39/DIKTI/Kep/1999. Setelah itu program studi Komputerisasi Akuntansi terus menerus berusaha memperbaiki diri dan meningkatkan kualitas dan statusnya.\n"
				+ "Program studi Komputerisasi Akuntansi mendapatkan Akreditasi BAN-PT dengan nilai B pada tahun 2012. Perolehan status tersebut merupakan bukti kemauan dari pihak akademis untuk selalu memberikan peningkatan kualitas pendidikan.", true);
		
		return view;
	}

}
