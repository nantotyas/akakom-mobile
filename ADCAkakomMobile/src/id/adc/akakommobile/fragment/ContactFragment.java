package id.adc.akakommobile.fragment;

import id.adc.akakommobile.R;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

public class ContactFragment extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_contact, container, false);
		
		 WebView myBrowser = (WebView)view.findViewById(R.id.akMap);
		  myBrowser.loadUrl("file:///android_asset/akakomMap.html");
		  myBrowser.getSettings().setJavaScriptEnabled(true);
		  
		return view;
	}

}
