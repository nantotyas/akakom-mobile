package id.adc.akakommobile;

import id.adc.akakommobile.R;
import id.adc.akakommobile.fragment.AboutFragment;
import id.adc.akakommobile.fragment.ContactFragment;
import id.adc.akakommobile.fragment.HomeFragment;
import id.adc.akakommobile.fragment.KMFragment;
import id.adc.akakommobile.fragment.NavigationDrawerFragment;
import id.adc.akakommobile.fragment.ProfilFragment;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends FragmentActivity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {

	private NavigationDrawerFragment mNavigationDrawerFragment;
	private CharSequence mTitle;
	public FragmentManager fragmentManager;
	static Boolean cekBackFragment=false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mTitle = getTitle();
		
		if (savedInstanceState == null || !cekBackFragment) {
			fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction().add(R.id.container, new HomeFragment()).commit();
		}
		
		if(!cekBackFragment) {
			mTitle = getString(R.string.title_home);
			fragmentManager.beginTransaction().replace(R.id.container, new HomeFragment()).commit();
		}

		mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));
		getActionBar().setBackgroundDrawable(
		        new ColorDrawable(Color.parseColor("#3366ff")));
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		
		switch (position) {
		case 1:
			mTitle = getString(R.string.title_home);
			fragmentManager.beginTransaction().replace(R.id.container, new HomeFragment()).commit();
			break;
		case 2:
			mTitle = getString(R.string.title_profil);
			fragmentManager.beginTransaction().replace(R.id.container, new ProfilFragment()).commit();
			break;
		case 3:
//			mTitle = getString(R.string.title_prodi);
//			fragmentManager.beginTransaction().replace(R.id.container, new ProdiFragment()).commit();
			Intent intentProdi = new Intent(MainActivity.this, ProdiActivity.class);
			startActivity(intentProdi);
			finish();
			break;
		case 4:
			mTitle = getString(R.string.title_km);
			fragmentManager.beginTransaction().replace(R.id.container, new KMFragment()).commit();
			break;
		case 5:
//			mTitle = getString(R.string.title_pmb);
//			fragmentManager.beginTransaction().replace(R.id.container, new PMBFragment()).commit();
			Intent intentPmb = new Intent(MainActivity.this, PMBActivity.class);
			startActivity(intentPmb);
			finish();
			break;
		case 6:
			mTitle = getString(R.string.title_contact);
			fragmentManager.beginTransaction().replace(R.id.container, new ContactFragment()).commit();
			break;
				
		}

	}
	

/*	public void onSectionAttached(int number) {
		switch (number) {
		case 1:
			mTitle = getString(R.string.title_home);
			fragment = new HomeFragment();
			break;
		case 2:
			mTitle = getString(R.string.title_profil);
			fragment = new ProfilFragment();
			break;
		case 3:
			mTitle = getString(R.string.title_prodi);
			break;
		case 4:
			mTitle = getString(R.string.title_pmb);
			break;
		case 5:
			mTitle = getString(R.string.title_contact);
			break;
		}
	}*/
	
	static void statusHome() {
		cekBackFragment=true;
	}
	
	public void actionAbout() {
		mTitle = "About";
		fragmentManager.beginTransaction().replace(R.id.container, new AboutFragment()).commit();
	}

	public void restoreActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.main, menu);
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_example) {
			actionAbout();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
