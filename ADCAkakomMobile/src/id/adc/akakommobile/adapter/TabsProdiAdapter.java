package id.adc.akakommobile.adapter;

import id.adc.akakommobile.fragment.KAFragment;
import id.adc.akakommobile.fragment.MIFragment;
import id.adc.akakommobile.fragment.SIFragment;
import id.adc.akakommobile.fragment.TIFragment;
import id.adc.akakommobile.fragment.TKFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TabsProdiAdapter extends FragmentPagerAdapter
{

	public TabsProdiAdapter(FragmentManager fragmentManager)
	{
		super(fragmentManager);
	}

	@Override
	public Fragment getItem(int index)
	{

		switch (index)
		{
			case 0:
				return new TIFragment();
			case 1:
				return new SIFragment();
			case 2:
				return new KAFragment();
			case 3:
				return new MIFragment();
			case 4:
				return new TKFragment();
		}

		return null;
	}

	@Override
	public int getCount()
	{
		// get item count - equal to number of tabs
		return 5;
	}

}
