package id.adc.akakommobile.adapter;

import id.adc.akakommobile.fragment.StrataFragment;
import id.adc.akakommobile.fragment.VokasiFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TabsPMBAdapter extends FragmentPagerAdapter{

	public TabsPMBAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int arg0) {
		// TODO Auto-generated method stub
		switch (arg0)
		{
			case 0:
				return new StrataFragment();
			case 1:
				return new VokasiFragment();
		}

		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 2;
	}

}
